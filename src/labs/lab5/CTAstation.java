package labs.lab5;

public class CTAstation {

	public static void main(String[] args) {
		//This class is used to store the values of latitude and longitude 
	    double lat;
		double lng;
		
		public GeoLocation() {
			lat= 1.0;
			lng= 1.0;
		}
		
		public GeoLocation(double lat, double lng) {
			setLat(lat);
			setlng(lng);
		}
		
		public double getlat() {
			return lat;
		}
		
		public double getLng() {
			return lng;
		}
		
		public void setLat(double lat) {
			this.lat = lat;
		}
		
		public void setLng(double lng) {
			this.lng = lng;
		}
		
		public String toString() {
			return "(lat , lng)";
			
		}
		
		public boolean Latif() {
			if (lat >= -90 && lat<= 90) {
				return true;
			}
			 return false ;
		}
		public boolean Lngif() {
			if (lat >= -180 && lat<= 180) {
				return true;
			}
			 return false ;
		}
		//A method called `calcDistance` that takes another `GeoLocation` and returns a double.
		public double calcDistance(GeoLocation pos) {
			double distance = Math.sqrt(Math.pow(this.lat-pos.lat,2)+Math.pow(this.lng-pos.lng,2));
			return distance;
		}
		// A method called `calcDistance` that takes a lng and lat and returns a double.
		public double calcDistance(double lat1, double lat2, double lng1, double lng2) {
			double distance = Math.sqrt(Math.pow(lat1-lat2, 2)+ Math.pow(lng1-lng2,2));
			return distance;
		}
	}
}

