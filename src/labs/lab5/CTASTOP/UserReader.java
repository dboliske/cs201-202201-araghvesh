package labs.lab5.CTASTOP;

import java.util.Scanner;

public class UserReader extends Reader {
	
	
	public UserReader() {
		super();
		setInput(new Scanner(System.in));
	}

	@Override
	public String nextLine() {
		if (this.input == null) {
			return null;
		}
	return this.input.nextLine();
	}

	@Override
	public int nextInt() {
		if (this.input == null) {
			return null;
		}
		
		while (true) {
			try {
				String in = input.nextLine();
				int value = Integer.parseInt(in);
				return value;
			} catch (Exception e) {
				System.out.println("please enter an option.");
			}
		}
	}

	@Override
	public double nextDouble() {
		if (this.input == null) {
			return null;
		}
		
		while (true) {
			try {
				String in = input.nextLine();
				double value = Double.parseDouble(in);
				return value;
			} catch (Exception e) {
				System.out.println("please enter a number .");
			}
		return 0;
	}

}
