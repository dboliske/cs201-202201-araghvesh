package labs.lab5.CTASTOP;

import java.io.File;
import java.util.Scanner;

public class FileReader extends Reader {
	
	private String fileName;
	
	public FileReader(String fn) {
		super();
		this fileName = fn;
		setInput(fn);
		
	}
	
	public void setInput(String fn) {
		try {
			File f = new File(fn);
			input = new Scanner(f);
		} catch (Exception e) {
			System.out.println("file do not exist");
		}
	}
	

	@Override
	public String nextLine() {
		if (input ==null ||  !input.hasNextLine()) {
			return null;
		}
		
		return input nextLine();
	}

	@Override
	public int nextInt() {
		 if (input ==null ||  !input.hasNextLine()) {
		 return null;
		 }
		 
		 String in = input.nextLine();
		 return integer .parseInt(in);
		 
		 
	}

	@Override
	public double nextDouble() {
		 if (input ==null ||  !input.hasNextLine()) {
			 return null;
			 }
			 
			 String in = input.nextLine();
			 return double .parseDouble(in);
		return 0;
	}

}
