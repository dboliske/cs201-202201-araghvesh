package labs.lab5.CTASTOP;

import java.util.Scanner;

public abstract class Reader {
private Scanner input;
	
	public Reader() {
		input = null;
	}

	public Scanner getInput() {
		return input;
	}

	public void setInput(Scanner input) {
		this.input = input;
	}
	
	public void close() {
		if(input != null) {
			input.close();
			input=null;
		}
	}
 
    
	public abstract String nextLine() ;
	public abstract int nextInt();
	public abstract double nextDouble();
	
	
}

}
