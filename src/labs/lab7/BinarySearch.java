package labs.lab7;

import java.util.Scanner;

public class BinarySearch {
		// Returns index of x if it is present in arr[],
		// else return -1
		static int binarySearch(String[] arr, String x)
		{
		int l = 0, r = arr.length - 1;
		while (l <= r) {
		int m = l + (r - l) / 2;

		int res = x.compareTo(arr[m]);

		if (res == 0) // Check if x is present at mid
		return m;

		if (res > 0) // If x greater, ignore left half
		l = m + 1;

		else // If x is smaller, ignore right half
		r = m - 1;
		}
		return -1;
		}
		public static void main(String []args)
		{
		String[] arr = { "c", "html", "java", "python", "ruby", "scala"};
		
		//let's import scanner method 
		Scanner sc= new Scanner(System.in); 
		System.out.print("Enter a string: ");
		
		String str= sc.nextLine(); //this line reads string
		int result = binarySearch(arr, str);

		if (result == -1)
		System.out.println("Element not present");
		else
		System.out.println("Element found at "
		+ "index " + result);
		}
	}
