package labs.lab7;

import java.util.Arrays;

public class NewBubblesort {

		  // to perform the bubble sort
		  static void bubbleSort(int array[]) {
		    int size = array.length;
		    
		    // loop to access each array element
		    for (int i = 0; i < (size-1); i++) {
		    
		      // to check if swapping occurs
		      boolean swapped = false;
		      
		      // loop to compare adjacent elements
		      for (int j = 0; j < (size-i-1); j++) {

		        // to compare two array elements
		        // change > to < to sort in descending order
		        if (array[j] > array[j + 1]) {

		          // swapping occurs if elements are not in the order
		          int temp = array[j];
		          array[j] = array[j + 1];
		          array[j + 1] = temp;
		          
		          swapped = true;
		        }
		      }
		      if (!swapped)
		        break;

		    }
		  }

		  public static void main(String args[]) {  
		    int[] data = { 10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		    bubbleSort(data);
		    System.out.println("Sort Array in Ascending Order:");
		    System.out.println(Arrays.toString(data));
		  }
	}
