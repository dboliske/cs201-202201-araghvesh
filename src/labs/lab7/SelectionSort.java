package labs.lab7;

public class SelectionSort {
	public static void selectionSort(double[]arr) {
		//array for sort element 
		for (int i=0; i<arr.length - 1; i++)
		{
			int index =i;
			for (int j=i+1; j<arr.length; j++) {
				if (arr[j]<arr[index])
				{
					//searching the lowest index
					index=j;
				}
			}
			double smallerNumber=arr[index];
			arr[index]=arr[i];
			arr[i]=smallerNumber;
		}
	}
	
	//main method 
	public static void main(String a[]) {
		
		//Given array 
		double[]arr1 = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		//to print the given array 
		System.out.println("Given Array:");
		System.out.print("[");
		for(double i:arr1)
		{
			System.out.print(i+" , ");
		}
		System.out.println("]");
		
		
		// sorting an array using selection sort method 
		selectionSort(arr1);
		
		//printing the sorted array
		System.out.println("\nSelection sort:");
		System.out.print("[");
		for(double i:arr1)
		{
			System.out.print(i+" , ");
		}
		System.out.print("]");
	}

}
	