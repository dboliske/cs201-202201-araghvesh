package labs.lab2;

import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
		int marks[] = new int[8]; // total  number of subjects is 8
        int i;
        float total=0, avg;
        Scanner scanner = new Scanner(System.in);
		
        
        for(i=0; i<8; i++) { 
           System.out.print("Enter Marks of Subject"+(i+1)+":"); //to enter marks of the subjects
           marks[i] = scanner.nextInt();
           total = total + marks[i];
        }
        scanner.close();
        //Calculating average here
        avg = total/8;
        System.out.print("The student Grade is: ");
        if(avg>=100)
        {
            System.out.print("A");
        }
        else if(avg>=80 && avg<100)
        {
           System.out.print("B");
        } 
        else if(avg>=60 && avg<80)
        {
            System.out.print("C");
        }
        else if (avg>=40 && avg<60)
        {
        }
        else
        {
            System.out.print("D");
        }
    }
}
