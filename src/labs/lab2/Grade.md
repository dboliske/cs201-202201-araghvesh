# Lab 2

## Total

16/20

## Break Down

* Exercise 1    6/6
* Exercise 2    3/6
* Exercise 3    5/6
* Documentation 2/2

## Comments
-Ex2 doesn't handle decimal points (-1 point)
-Ex2 only works for 8 grades, no less or more (-1 point)
-Ex2 doesn't give the user the option to terminate with -1 (-1 point)
-Ex3 doesn't handle decimal points (-1 point)
-Ex3 doesn't tell the user that 4 is to exit
