package labs.lab2;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		System.out.println("Enter a number:"); //prompting user for a number 
		
		//  creating scanner 
		 Scanner in = new Scanner(System.in);
		 
		 //printing out square for the given dimensions
	        int n = in.nextInt();
	        for(int i = 0; i < n; ++i) {
	            for(int j = 0; j < n; ++j) {
	                if(i == 0 || j == 0 || i == n-1 || j == n-1) {
	                    System.out.print("*");
	                } else {
	                    System.out.print("*");
	                }
	            }
	            System.out.println();
	}

}
}
