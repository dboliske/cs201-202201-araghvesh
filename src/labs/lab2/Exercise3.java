package labs.lab2;

import java.util.Scanner;

public class Exercise3 {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);  //create a scanner for user input
		
		 //Declaring all variables
	        int a,b,c;
	        int choice;
	        Scanner scanner = new Scanner(System.in);
	        //Creating infinite while loop
	        while(true) {
	            
	            //Asking user to make choice
	            System.out.print("Make your choice (1) , (2) , (3) :");
	            choice = scanner.nextInt();
	 
	            //Creating switch case branch
	            switch (choice) {
	 
	                //First case for finding the addition
	                case 1:
	                    System.out.println(" hello ");
	                    break;
	 
	                //Second case for adding two numbers
	                case 2:
	                    System.out.print("Enter the first number: ");
	                    a = scanner.nextInt();
	                    System.out.print("Enter the second number:");
	                    b = scanner.nextInt();
	                    c = a + b;
	                    System.out.print("The addition of the numbers is = " + c +"\n");
	                    break;
	 
	                //Third case for finding the product or multiplication of two numbers
	                case 3:
	                    System.out.print("Enter the first number:");
	                    a = scanner.nextInt();
	                    System.out.print("Enter the second number:");
	                    b = scanner.nextInt();
	                    c = a * b;
	                    System.out.print("The product of the numbers is = " + c + "\n");
	                    break;   
	                case 4:
	                    System.exit(0);
	 
	                //default case to display the message invalid choice made by the user
	                default:
	                    System.out.println("Invalid choice!!! Please make a valid choice. \\n\\n");
	            }
	        }
	    }
}
