package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class Que {

	    //public static void main(String[] args) {

		// creating scanner 
		Scanner sc = new Scanner(System.in); 
	    
		// Adding customer list which will take it as Array list as an argument
		public int addCustomer(ArrayList<String>customerQueue) {
			//taking name of the customer 
			System.out.print("Enter the name of customer:");
			
			
			sc.nextLine();
			//Object sc;
			//extra sc.nextline() is important to break sc.nextInt()
			//((Scanner) sc).nextLine();
			
			//taking the input for the name of customer 
			String name = sc.nextLine();
			
			//Adding the customer name to the end of the queue
			customerQueue.add(name);
			
			// Returning the customer position
			return customerQueue.size();
		}
		
		// The method that helps customer take Array list as an argument 
		public void helpCustomer(ArrayList<String> customerQueue) {
			
			// Removing first customer from the Queue
			String customerName = customerQueue.remove(0);
			
			//printing customer's name
			System.out.println(customerName);	
		}
		
		//method menu which prints menu
		//taking array list as an argument 
		public void menu(ArrayList<String>customerQueue) {
			
			//initializing choice with 0
			int choice = 0;
			
			//while choice is not 3
			while(choice != 3) {
				
				//printing the menu
				System.out.print("Enter your choice:");
				System.out.print("1. Add customer to queue:");
				System.out.print("2. Help customer :");
				System.out.print("3. Exit:");
				
				//taking the choice input
				//Scanner sc;
				choice = sc.nextInt();
				
				
			if (choice ==1) {
				addCustomer(customerQueue);
				
			}
				//if the choice is other than 1,2 and 3 print invalid input
			else if (choice != 3) {
				System.out.println("please enter a valid input");
			}
				
			}
			
		}
			//driver code
			public static void main(String[] args) {				
				//
			ArrayList<String> customerQueue= new ArrayList<String>();
			Que deli = new Que();
			deli.menu(customerQueue);
			
			}
			
	}
	

