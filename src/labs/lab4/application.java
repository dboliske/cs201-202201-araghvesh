package labs.lab4;

public class application
{
  
   public static void main(String[] args)
   {
       //Creating object using default constructor
       GeoLocation location1=new GeoLocation();
       location1.setLat(115.1); //setting values using mutator method
       location1.setLng(89.7); //setting value using mutator method
      
       //Hardcoding values of lat and lng to create new GeoLocation object
       double lat=115.1;
       double lng=89.7;
      
       //Creating object using non-default constructor
       GeoLocation location2=new GeoLocation(lat,lng);
      
       System.out.println("Location 1:");
      
       System.out.println("Lat:"+location1.getLat()); //printing values using accessor method
       System.out.println("Lng:"+location1.getLng()); //printing values using accessor method
      
       System.out.println("\nLocation 2:");
      
       System.out.println("Lat:"+location2.getLat()); //printing values using accessor method
       System.out.println("Lng:"+location2.getLng()); //printing values using accessor method
      
   }
}