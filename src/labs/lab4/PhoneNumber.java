package labs.lab4;

public class PhoneNumber {
	
	
	// three instances variables will be 
	private String countryCode;
	private String areaCode;
	private String number;
	
	//default constructor will be 
	public PhoneNumber() {
		
	}
    
	//non-default constructor will be 
	public PhoneNumber(String countryCode, String areaCode , String number){
		this.countryCode = countryCode;
		this.areaCode = areaCode;
		this.number = number;
			
	}
	
	
	// the three accessor method will be 
	public String getcountryCode() {
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	
	//the three mutator methods will be 
	public void setcountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public void setareaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public void setnumber(String number) {
		this.number= number;
	}
	
	
	
	
    //a method that will return the entire phone number as a single string (the `toString` method).
	
	public String toString () {
		return countryCode + areaCode +  number;
	}
	
	//method that will return `true` if the `areaCode` is 3 characters long
	public boolean validatiereaCode() {
	if (areaCode.length() != 3 ) {
		 return false ; 
	}
	
	try {
		int i = Integer.parseInt(areaCode);
	} 
	catch (NumberFormatException e) {
		System.out.println("invalid area code");
		return false ;
	}
	
	return true;
	}
	
	//method that will return `true` if the `number` is 7 characters long.
	public boolean validateareaCode() {
		if (number.length() != 7) {
			 return false ; 
		}
		
		try {
			int i = Integer.parseInt(number);
		} 
		catch (NumberFormatException e) {
			System.out.println("invalid number ");
			return false ;
		}
		
		return true;
		}

	public boolean validateNumber() {
		if (number.length() !=7)
			return false;
		try {
			int i = Integer.parseInt(number);
		} 
		catch(NumberFormatException e) {
			System.out.println("Invalid Number");
			return false;
		}
		return true;
		}
	}


	//
	
	
	//an application class that instantiates two instances of `PhoneNumber`
	 class App {
		public static void main (String arg[]) {
			
			//use default constructor 
			PhoneNumber pn1 = new PhoneNumber();
			//use mutator methods to set its values 
			
			pn1.setcountryCode("91");
			pn1.setareaCode("312");
			pn1.setnumber("15678392");
			
			//using a non-default constructor 
			PhoneNumber pn2 = new
			PhoneNumber("91", "312", "15678392");
			
			
			
			//display the values of each objects using a tostring method 
			System.out.println(pn1.toString() );
			System.out.println(pn2.toString());
			
			
			
			
			//to test the validation 
			System.out.println("Phone Number 1 area code valid: "+pn1. validatiereaCode()); 
			System.out.println("PhoneNumber 1 number valid: "+pn1. validateNumber()); 
			System.out.println("Phone Number 2 area code valid: "+pn2. validatiereaCode()); 
			System.out.println("Phone Number 2 number valid: "+pn2. validateNumber());

			
			
			
	        }
		
	
	
	
}
