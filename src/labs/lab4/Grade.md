# Lab 4

## Total

25/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        8/8
  * Application Class   1/1
* Part II PhoneNumber
  * Exercise 1-9        8/8
  * Application Class   0/1
* Part III 
  * Exercise 1-8        5/8
  * Application Class   0/1
* Documentation         3/3

## Comments
- Potion class has errors: shouldn't have main function, has class declarations instead of constructors, and does not implement all methods.
- In part 1, refrain from calling super(), GeoLocation is not an inherited class. You want to use this() instead.
- Refrain from using empty constructors, have the default constructor set default values.