package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) throws IOException {
		
		    List<String> list = new ArrayList<String>(); //creating the list of arrays 
		    
		    // creating scanner
	        Scanner numbers = new Scanner(System.in);

	        do {
	            System.out.println("Current list is " + list); //to print current list of arrays
	            System.out.println("Add Values to Array (Yes / Done)");// to add values to the array
	            if (numbers.next().startsWith("Yes")) {
	                System.out.println("Enter the Value to add to Array: ");
	                list.add(numbers.next());
	            } else {
	                break;
	            }
	        } while (true);
	        numbers.close();
	        System.out.println("List is " + list);
	        String[] arr = list.toArray(new String[0]);
	        FileWriter myWriter = new FileWriter("output.txt"); //to create a file
	        myWriter.write("Array is " + Arrays.toString(arr));
	        System.out.println("Successfully wrote to the Array values to output.txt file.");

	        myWriter.close();

	}

}
