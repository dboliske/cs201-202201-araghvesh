package labs.lab1;
import java.util.Scanner;
public class Exercise5 {

    public static void main(String[] Strings) {

        Scanner input = new Scanner(System.in);

        //converting my height inches to centimeters
        System.out.print("Enter Inches = ");
        double inch = input.nextDouble();
        double cm = inch * 2.54;
        System.out.println( cm + " Centimeters");

    }
}