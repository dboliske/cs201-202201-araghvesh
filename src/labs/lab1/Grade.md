# Lab 1

## Total

6/20

## Break Down

* Exercise 1    2/2
* Exercise 2    2/2
* Exercise 3    0/2
* Exercise 4
  * Program     0/2
  * Test Plan   0/1
* Exercise 5
  * Program     0/2
  * Test Plan   0/1
* Exercise 6
  * Program     0/2
  * Test Plan   0/1
* Documentation 2/5

## Comments
Please read the README.md file carefully. Exercises 2 through 6 in your submission only cover the 2nd Exercise, and they should all be in the same Java class.