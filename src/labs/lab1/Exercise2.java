package labs.lab1;

import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
		
		//performing arithmetic operations
		//create scanner 
		Scanner input  = new Scanner(System.in);
		
		//prompt for x
		System.out.print("X:");
		double x = Double.parseDouble(input.nextLine());
		
		//prompt for y
		System.out.print("Y:");
		double y = Double.parseDouble(input.nextLine());
		
		System.out.print("Operation:");
		char op = input.nextLine().charAt(0);
		
		if (op == '+') {
			System.out.println("x+y=" +(x+y)); //operation for adding two numbers
		} else if (op == '-') {
				System.out.println("x-y=" +(x-y)); //operation for substituting two numbers
		} else if (op == '*') {
				System.out.println("x*y=" +(x*y));	//operation for multiplying two numbers
		} else if (op == '/') {
			System.out.println("x/y=" +(x/y));      //operation for dividing two numbers
	    } else {
		System.out.println("unknown operation");
	    }
	}
}
		
