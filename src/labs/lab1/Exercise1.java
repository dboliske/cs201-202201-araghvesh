package labs.lab1;

import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) {
		// to print out the user input 
		
		Scanner input = new Scanner (System.in);
		
		//prompt the user for the name 
		System.out.print("Prompt User name :");
		
		//read in the next line of text typed user
		String value = input.nextLine();
			
		
		//print out what the user entered 
		System.out.println("Echo: " + value );
		

	}

}
