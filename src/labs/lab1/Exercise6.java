package labs.lab1;

import java.util.Scanner;

public class Exercise6 {

	public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        //converting my height inches to centimeters
        System.out.print("Enter Inches = ");
        int inch = input.nextInt();
        double feet;
        feet = inch / 12;
        System.out.println( feet + " feet");
	}

}
