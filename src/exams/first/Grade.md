# Midterm Exam

## Total

85/100

## Break Down

1. Data Types:                  18/20
    - Compiles:                 5/5
    - Input:                    3/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  17/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               2/5
4. Arrays:                      20/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  5/5
5. Objects:                     10/20
    - Variables:                3/5
    - Constructors:             2/5
    - Accessors and Mutators:   2/5
    - toString and equals:      3/5

## Comments

1. Good, but doesn't check that the input is an integer.
2. Good
3. Good, but doesn't check that the input is an integer and doesn't quite match the required output.
4. Good
5. Not a clearly implemented class with many methods missing details.
