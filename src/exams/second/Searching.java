package exams.second;

import java.util.Scanner;

public class Searching {
	
		public static int jumpSearch(double [] arr, double x)
	    {
	        int n = arr.length;
	 
	        // Finding block size to be jumped
	        int step = (int)Math.floor(Math.sqrt(n));
	 
	        // Finding the block where element is
	        // present (if it is present)
	        int prev = 0;
	        while (arr[Math.min(step, n)-1] < x)
	        {
	            prev = step;
	            step += (int)Math.floor(Math.sqrt(n));
	            if (prev >= n)
	                return -1;
	        }
	 
	        // Doing a linear search for x in block
	        // beginning with prev.
	        while (arr[prev] < x)
	        {
	            prev++;
	 
	            // If we reached next block or end of
	            // array, element is not present.
	            if (prev == Math.min(step, n))
	                return -1;
	        }
	 
	        // If element is found
	        if (arr[prev] == x)
	            return prev;
	 
	        return -1;
	    }
	 
	    // Driver program to test function
	    public static void main(String [ ] args)
	    {
	    	double[] lang = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142}; // these are the set of arrays
			
			Scanner input = new Scanner(System.in);
			System.out.print("Search the term: ");
			double value = input.nextDouble();
			int index = jumpSearch(lang, value);
			if (index == -1) {
				System.out.println(" -1");
			} else {
				System.out.println(value + " Is at index " + index + ".");
			}

			input.close();
	    }
	}