package exams.second;

import java.util.Scanner;

public class ArrayList {
	public static void main(String[] args) {
		//create scanner for user input
		Scanner input = new Scanner(System.in);
		
		double[] values = new double[6];
		for(int i=0; i<values.length; i++) {
			System.out.print("(" + (i+1) + ") number: ");
			values[i] = Double.parseDouble(input.nextLine());
		}
		
		boolean done = false;
		
		do {
			System.out.println("1. Min"); // print statement to print the minimum
			System.out.println("2. Max"); // print statement to print the maximum
			System.out.println("3. Exit"); // to print the exit
			
			System.out.print("Choice: ");
			String choice = input.nextLine();
			switch(choice) {
				case "1": // choice for minimum
					double min = values[0];
					for (int i=1; i<values.length; i++) {
						if (values[i] < min) {
							min = values[i];
						}
					}
					System.out.println("Min: " + min);
					break;
				case "2": // choice for maximum
					double max = values[0];
					for (int i=1; i<values.length; i++) {
						if (values[i] > max) {
							max = values[i];
						}
					}
					System.out.println("Max: " + max);
					break;
				case "3": // to exit the program
					done = true;
					break;
				default:
					System.out.println("DONE");
			}
		} while (!done);
		
		input.close();
	}

}
 
	