package exams.second;

public class ComputerLab {
	boolean computers;

    public ComputerLab() { // constructor
    }

    public boolean getComputers() { // using Accessors 
        return computers;
    }

    public void setComputers(boolean computers) { //using mutators
        this.computers = computers;
    }

    @Override
    public String toString() { //to string method
        return "ComputerLab{" + "computers=" + computers + '}';
    }
}
