package exams.second;

public class Rectangle extends Polygon {
	
	private double x; // let x be the width 
    private double y; // let y be the height 

    public Rectangle() // inserting the constructor
    {
        super(); 
        x = 1;
        y = 1;
    }

    public double getWidth()  // inserting the accessor method to the width of rectangle
    {
        return x;
    }

    public double getHeight() //inserting the accessor method to the height of rectangle
    {
        return y;
    }

    public void setWidth(double x) //inserting the mutator method to the width of rectangle
    {
        this.x = x;
    }

    public void setHeight(double y) //inserting the mutator method to the height of rectangle
    {
        this.y = y;
    }


    @Override
    public String toString() // inserting the to string method 
    {
        return "[Name: " + getName() + " Width: " + x + " Height: " + y + "]";
    }

    @Override
    public double area() // the area of the reactangle 
    {
        return y * x;
    }

    @Override
    public double perimeter()
    {
        return 2.0 * (y + x); // the perimeter of the rectangle 
    }
}
