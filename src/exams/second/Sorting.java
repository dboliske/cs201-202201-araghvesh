package exams.second;

public class Sorting {
	// Sorting of an array of strings
	static void selectionSort(String arr[],int n)
	{
	    // So we sort One by one move boundary of unsorted subarray
	    for(int i = 0; i < n - 1; i++)
	    {
	     
	        // here we Find the minimum element in unsorted array
	        int min_index = i;
	        String minStr = arr[i];
	        for(int j = i + 1; j < n; j++)
	        {
	             
	            /*compareTo() will return a -ve value,
	            if string1 (arr[j]) is smaller than string2 (minStr)*/
	            // If arr[j] is smaller than minStr
	         
	            if(arr[j].compareTo(minStr) < 0)
	            {
	                // Make arr[j] as minStr and update min_idx
	                minStr = arr[j];
	                min_index = j;
	            }
	        }
	    // So here we are Swapping the minimum element
	    // found with the first element.
	    if(min_index != i)
	    {
	        String temp = arr[min_index];
	        arr[min_index] = arr[i];
	        arr[i] = temp;
	    }
	    }
	}
	 
	// this is the  Driver code
	public static void main(String args[])
	{
	    String arr[] = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
	    int n = arr.length;
	        System.out.println("Given array is");
	          
	    // Printing the array before sorting
	    for(int i = 0; i < n; i++)
	    {
	        System.out.println(i+": "+arr[i]);
	    }
	    System.out.println();
	    selectionSort(arr, n);
	    System.out.println("Sorted array is");
	     
	    // Printing the array after sorting
	    for(int i = 0; i < n; i++)
	    {
	        System.out.println(i+": "+arr[i]);
	    }
	}
	}
