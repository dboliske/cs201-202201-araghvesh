package exams.second;

public class Polygon {
	private String name;

    public Polygon() // inserting the constructor
    {
        name = "triangle";
    }

    public String getName() // inserting the accessor method
    {
        return name;
    }

    public void setName(String name) // inserting the mutator
    {
        this.name = name;
    }

    public String toString() // the tostring method
    {
        return "Name: " + name ;
    }

    public double area() // the area 
    {
        return 0;
    }

    public double perimeter() // the perimeter
    {
        return 0;
    }
}
