package exams.second;

public class Circle extends Polygon {
        private double r; // r is the radius 

        public Circle() // inserting the constructors 
        {
            super();
            r = 1;
        }

        public void setRadius(double x) // inserting the mutators for the radius 
        {
            r = x;
        }

        public double getRadius() // inserting the acessors for the radius
        {
            return r;
        }

        @Override
        public String toString() // inserting the to string 
        {
            return "Name: " + getName() + " Radius: " + r;
        }

        @Override
        public double area() // the area of the circle 
        {
            return  Math.PI*r*r;    
        }

        @Override
        public double perimeter() // the perimeter of the circle 
        {
            return 2.0 * Math.PI * r ;
        }

}
