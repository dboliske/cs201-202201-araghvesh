package exams.second;

public class ClassRoom {
		protected String building;
	    protected String roomNumber;
	    int seats;

	    public ClassRoom() {   // constructors 
	    }

	    public String getBuilding() { //inserting the accessor 
	        return building;
	    }

	    public void setBuilding(String building) { // inserting the mutators
	        this.building = building;
	    }

	    public String getRoomNumber() { //inserting the accessor
	        return roomNumber;
	    }

	    public void setRoomNumber(String roomNumber) { //inserting the mutators
	        this.roomNumber = roomNumber;
	    }

	    public int getSeats() { //inserting the accessors
	        return seats;
	    }

	    public void setSeats(int seats) { //inserting the mutators
	            this.seats = seats;
	        }

	    @Override
	    public String toString() {
	        return "Classroom{" + "building=" + building + ", roomNumber=" + roomNumber + ", seats=" + seats + '}';

	}

}
