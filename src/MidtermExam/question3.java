package MidtermExam;

import java.util.Scanner;

public class question3 {

	public static void main(String[] args) {
System.out.print("Enter a number:"); //prompting user for a number 
		
		//  creating scanner 
		 Scanner in = new Scanner(System.in);
		 
		 
		//printing out triangle for the given dimensions
		 int n = in.nextInt(); // let n be the number of rows
			    for (int i = n ; i >= 1; i--) {
			      for (int j = 1; j > i; j--) {
			    	   System.out.print(" ");
			    	   }
			      for (int j=1 ; j<=i ; j++ ) {
			      System.out.print("*");
			      }
			      System.out.println();
			    }
			  }
}
