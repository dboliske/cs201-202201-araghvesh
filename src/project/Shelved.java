package project;

public class Shelved extends Item {
	//AKANKSHA RAGHVESH
	//22/04/2022
	// This class is defined for shelved items like pencils , pen , pad of paper.
	
	private String name;
	private double price;
	
	public Shelved(String values, int i, double d, boolean b) {
		this.name = "pencil";
		this.price = 1.29;
	}
	
	public Shelved(String name, double price) {
		this.name = name;
		this.price = 1.29;
		setprice(price);
	}
	//
	
	// mutators 
	
	public void setName(String name) {
		this.name = name;
	}
	public void setprice(double price) {
		if (price > 0) {
			this.price = price;
		}
	}
	// accessor method 
	public String getName() {
		return name;
	}
	
	//accessors method
	public double getPrice() {
		return price;
	}
	
	@Override
	public boolean equals(Object obj) { // the equals method 
		if (!(obj instanceof Shelved)) {
			return false;
		}
		
		Shelved a = (Shelved)obj;
		if (!this.name.equals(a.getName())) {
			return false;
		} else if (this.price != a.getPrice()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() { // the to string method 
		return name + " is " + price + " dollars";
	}
	
	protected String csvData() {
		return name +  "," + price;
	}
	
	public String toCSV() {
		return "Shelved," + csvData();
	}

	}
