package project;

//AKANKSHA RAGHVESH
	// 28/04/2022
	// THIS CLASS EXPLAINS ABOUT THE ITEMS IN THE STORE 

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Item {
	public static int length;
	// AKANKSHA RAGHVESH
	// 28/04/2022
	// THIS CLASS EXPLAINS ABOUT THE LIST THE NAME OF ITEM , PRICE OF ITEM , AVAIALABILITY , QUANTITY 
	private String name;
	private int age;
	private double price;
	private int expiryDate;
	
	public Item() {
		this.name = "item";
		this.age = 1;
		this.price = 1.0;
		this.expiryDate =  04/29/2022;
	}
	
	public Item(String name, int age, double price , int expiryDate) {
		this.name = name;
		this.age = 1;
		this.price = 1.0;
		this.expiryDate = 04/29/200;
		setAge(age);
		this.price = 1.0;
		setPrice(price);
	}
	// inserting mutators 
	private void setPrice(double price2) {
		// TODO Auto-generated method stub
		
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if (age >= 0) {
			this.age = age;
		}
	}
	
	public void setPrice1(double price) {
		if (price > 0) {
			this.price = price;
		}
	}
		
		public void setDate(int date) {
			if (date >= 04/29/2022) {
				this.expiryDate = date;
			}
	}
	
		//inserting the accessors
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public double getPrice() {
		return price;
	}
	public int getDate() {
		return expiryDate;
	}
	
	@Override
	public boolean equals(Object obj) {  // inserting the boolean equals method 
		if (!(obj instanceof Item)) {
			return false;
		}
		
		Item a = (Item)obj;
		if (!this.name.equals(a.getName())) {
			return false;
		} else if (this.age != a.getAge()) {
			return false;
		} else if (this.price != a.getPrice()) {
			return false;
		}
		else if (this.expiryDate != a.getDate()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() { // inserting the to string method 
		return name + " is " + age + price + " dollars.";
	}
	
	protected String csvData() {
		return name + "," + age + "," + price;
	}
	
	public String toCSV() {
		return "List," + csvData();
	}  
	
	// this is to read and write the csv file
	public static void main(String[] args) throws IOException {
		File file = new File("src/project/stock.csv");
		Scanner input = new Scanner(file); // read data from file
		// display the items present in the store
		
		while (input.hasNextLine()) {
			System.out.println(input.nextLine());
		}
		
		input.close();
	}

	public String getAge1() {
		// TODO Auto-generated method stub
		return null;
	}

}
