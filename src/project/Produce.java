package project;

public class Produce extends Item {
	// AKANKSHA RAGHVESH
	//22/04/2022
	// This class is defined for the produced items like banana , carrots etc. 
	
	private String name;
	private int expiryDate;
	private double price;
	
	public Produce() {
		this.name = "Banana";
		this.expiryDate = 04/29/2022;
		//SimpleDateFormat DateFor = new SimpleDateFormat("MM/dd/yyyy");
		this.price = 0.62 ;
	}
	
	public Produce(String name, int expiryDate, double price) {
		this.name = name;
		this.expiryDate = 04/29/2022;
		setDate();
		this.price = 0.62;
		setprice(price);
	}
	
	// inserting the mutators
	private void setDate() {
		// TODO Auto-generated method stub
		
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setDate(int date) {
		if (date >= 04/29/2022) {
			this.expiryDate = date;
		}
	}
	
	public void setprice(double price) {
		if (price > 0) {
			this.price = price;
		}
	}
	
	//inserting the accessors 
	public String getName() {
		return name;
	}
	
	public int getDate() {
		return expiryDate;
	}
	
	public double getPrice() {
		return price;
	}
	
	@Override
	public boolean equals(Object obj) {  //inserting the boolean equals method
		if (!(obj instanceof Produce)) {
			return false;
		}
		
		Produce a = (Produce)obj;
		if (!this.name.equals(a.getName())) {
			return false;
		} else if (this.expiryDate != a.getDate()) {
			return false;
		} else if (this.price != a.getPrice()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {   //inserting the to string method
		return name + " is " + expiryDate+ "  is " + price + " dollars";
	}
	
	protected String csvData() {
		return name + "," + expiryDate  + "," + price;
	}
	
	public String toCSV() {
		return "Produce" + csvData();
	}
}

