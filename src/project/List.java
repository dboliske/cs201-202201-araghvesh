package project;

//akanksha raghvesh
//29/04/2022
//this is the list class
public class List {
	public static <item> void main(String[] args) {
		List a = new List();
		Produce c = new Produce();
		Shelved d = new Shelved(null, 0);
		Aged e = new Aged(null, 0, 0, false);
		
		System.out.println(a.toString());
		System.out.println(c.toString());
		System.out.println(d.toString());
		System.out.println(e.toString());
		
		System.out.println(a.equals(c));
		System.out.println(a.equals(d));
		System.out.println(a.equals(e));
		System.out.println(c.equals(a));
		System.out.println(d.equals(a));
		System.out.println(e.equals(a));
		System.out.println(c.equals(e));
		System.out.println(e.equals(d));
		System.out.println(e.equals(c));
		System.out.println(d.equals(e));
		
		
		List[] Things = new List[4];
		Things[0] = a;
		Things[1] = c;
		Things[2] = d;
		Things[3] = e;
		
		for (int i=0; i<Things.length; i++) {
			System.out.println(Things[i].toString());
		}
	}

}
