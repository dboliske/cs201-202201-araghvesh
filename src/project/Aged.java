package project;

public class Aged extends Item{
	// AKANKSHA RAGHVESH
	// 22/04/2022
	// Description of the class is that this class has the data that's limited to particular age group 
	
	private String name;
	private int age;
	private double price;
	
	public Aged(String values, int i, double d, boolean b) {
		this.name = "wine";
		this.age = 21;
		this.price = 12.99;
	}
	
	public Aged(String name, int age, double price) {
		this.name = name;
		this.age = 21;
		setAge(age);
		this.price = 12.99;
		setprice(price);
	}
	
	public void setName(String name) { // inserting the mutators for name
		this.name = name;
	}
	
	public void setAge(int age) {     // inserting the mutators for age
		if (age >= 0) {
			this.age = age;
		}
	}
	
	public void setprice(double price) {  // inserting the mutators for price
		if (price > 0) { 
			this.price = price;
		}
	}
	
	public String getName() { // inserting accessor for name 
		return name;
	}
	
	public int getAge() {// inserting accessor for age
		return age;
	}
	
	public double getPrice() {// inserting accessor for price
		return price;
	}
	
	@Override
	public boolean equals(Object obj) {  // boolean equals method
		if (!(obj instanceof Aged)) {
			return false;
		}
		
		Aged a = (Aged)obj;
		if (!this.name.equals(a.getName())) {
			return false;
		} else if (this.age != a.getAge()) {
			return false;
		} else if (this.price != a.getPrice()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() {   // the to string method 
		return name + " is " + age + " for years old and is " + price + " dollars";
	}
	
	protected String csvData() {
		return name + "," + age + "," + price;
	}
	
	public String toCSV() {
		return "Aged," + csvData();
	}

	}

