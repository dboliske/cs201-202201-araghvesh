package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;
public class StoreApp {
		private static int age;

		public static Item[] readFile(String filename) {
			Item[] item = new Item[10];
			int count = 0;
			
			try {
				File f = new File(filename);
				
				// creating a scanner input 
				Scanner input = new Scanner(f);
				// end-of-file loop
				while (input.hasNextLine()) {
					try {
						String line = input.nextLine();
						String[] values = line.split(",");
						Item a = null;
						switch (values[0].toLowerCase()) {
							case "produce":
								a = (Item) new Produce(
									values[1],
									Integer.parseInt(values[2]),
									Double.parseDouble(values[3])
								);
								break;
							case "Shelved":
								a = (Item) new Shelved(
										values[1],
										Integer.parseInt(values[2]),
										Double.parseDouble(values[3]),
										Boolean.parseBoolean(values[4])
									);
								break;
							case "":
								a = (Item) new Aged(
										values[1],
										Integer.parseInt(values[2]),
										Double.parseDouble(values[3]),
										Boolean.parseBoolean(values[4])
							);
								break;
						}
						
						if (item.length == count) {
							item = resize1(item, item.length*2);
						}
						
						item[count] = a;
						count++;
					} catch (Exception e) {
						
					}
				}
				
				input.close();
			} catch (FileNotFoundException fnf) {
				// Do nothing.
			} catch(Exception e) {
				System.out.println("Error occurred reading in the file.");
			}
			
			item = resize1(item, count);
			
			return item;
		}
		
		public static Item[] resize1(Item[] data, int size) {
			Item[] temp = new Item[size];
			int limit = data.length > size ? size : data.length;
			for (int i=0; i<limit; i++) {
				temp[i] = data[i];
			}
			return temp;
		}
		public static Item[] menu(Scanner input, Item[] data) {
			boolean done = false;
			
			do {
				System.out.println("1. Add item");
				System.out.println("2. Remove item");
				System.out.println("3. List item");
				System.out.println("4. Exit");
				System.out.print("Choice: ");
				String choice = input.nextLine();
				switch (choice) {
				case "1": // to Add a new items
					data = addItem(data, input);
					break;
				case "2": //  to Delete the items
					data = deleteItem(data, input);
					break;
				case "3": // List Items in the store
					listItem(data);
					break;
				case "4": // Exit
					done = true;
					break;
				default:
					System.out.println("Invalid input , please try again:");
			}
		} while (!done);
		
		return data;
		public static Item[] createItem(Item[] data, Scanner input) {
			
			Item a;
			System.out.print("Item Name: "); // to print the name of the item
			String name = input.nextLine();
			System.out.print("Item Price: "); // to print the price of the item
			int price = 0;
			try {
				price = Integer.parseInt(input.nextLine());
			} catch (Exception e) {
				System.out.println("That is not a valid price. Returning to menu.");
				return data;
			}
			System.out.print("Item date: "); // to print the date of the item
			int date = 0;
			try {
				Integer Int;
				date = Integer.parseInt(input.nextLine());
			} catch (Exception e) {
				System.out.println("That is not a valid date. Returning to menu.");
				return data;
			}
			
			System.out.print("Type of Item(Shelved, Produce, or Aged): ");
			String type = input.nextLine();
			switch (type.toLowerCase()) {
				case "shelved":
					System.out.print("shelved fed (y/n): ");
					boolean fed = false;
					String yn = input.nextLine();
					switch (yn.toLowerCase()) {
						case "y":
						case "yes":
							fed = true;
							break;
						case "n":
						case "no":
							fed = false;
							break;
						default:
							System.out.println("That is not yes or no. Returning to menu.");
							return data;
					}
					a = new Shelved(name, price, date, fed);
					break;
				case "Aged":
					System.out.print("Aged hibernating (y/n): ");
					boolean hibernating = false;
					String yorn = input.nextLine();
					switch (yorn.toLowerCase()) {
						case "y":
						case "yes":
							hibernating = true;
							break;
						case "n":
						case "no":
							hibernating = false;
							break;
						default:
							System.out.println("That is not yes or no. Returning to menu.");
							return data;
					}
					a = (Item) new Aged(name, age, price, hibernating);
					break;
				case "produce":
					a = new Produce(name, price, date);
					break;
				default:
					System.out.println("That is not a valid item. Returning to menu.");
					return data;
			}
			data = resize(data, data.length+1);
			data[data.length - 1] = a;
			
			return data;
		}
		public static void listItem1(Item[] data) {
			for (int i=0; i<data.length; i++) {
				System.out.println(data[i]);
			}
		}
		
		public static Item[] deleteItem1(Item[] data, Scanner input) {
			Item[] temp = new Item[10];
			int count = 0;
			for (int i=0; i<data.length; i++) {
				boolean sell = yesNoPrompt("Add " + data[i].getName(), input);
				if (!sell) {
					if (temp.length == count) {
						temp = resize(temp, temp.length * 2);
					}
					temp[count] = data[i];
					count++;
				}
			}
			
			temp = resize(temp, count);
			
			return temp;
		}
		
		public static boolean yesNoPrompt(String prompt, Scanner input) {
			System.out.print(prompt + " (y/n): ");
			boolean result = false;
			String yn = input.nextLine();
			switch (yn.toLowerCase()) {
				case "y":
				case "yes":
					result = true;
					break;
				case "n":
				case "no":
					result = false;
					break;
				default:
					System.out.println("That is not yes or no. Using no.");
			}
			
			return result;
		}
		
		public static void saveFile(String filename, Item[] data) {
			try {
				FileWriter writer = new FileWriter(filename);
				
				for (int i=0; i<data.length; i++) {
					writer.write(data[i].toCSV() + "\n");
					writer.flush();
				}
				
				writer.close();
			} catch (Exception e) {
				System.out.println("Error saving to file.");
			}
		}

		public static void main(String[] args) {
			Scanner input = new Scanner(System.in);
			System.out.print("Load file: ");
			String filename = input.nextLine();
			// To Load file (if exists)
			Item[] data = readFile(filename);
			// Menu
			data = menu(input, data);
			// To Save file
			saveFile(filename, data);
			input.close();
	}
			private static void listItem(Item[] data) {
			// TODO Auto-generated method stub
			}
			private static Item[] deleteItem(Item[] data, Scanner input) {
			// TODO Auto-generated method stub
			return null;
			}
			private static Item[] addItem(Item[] data, Scanner input) {
			// TODO Auto-generated method stub
			return null;
		}

		private static Item[] resize(Item[] item, int i) {
			// TODO Auto-generated method stub
			return null;
		}
	}
	